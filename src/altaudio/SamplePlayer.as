package altaudio 
{
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	/**
	 * ...
	 * @author Hammerzeit
	 */
	public class SamplePlayer 
	{
		// Vector of registered SoundData instances:
		private var registeredSounds:Vector.<SoundData>;
		// Vector of active SoundData instances:
		private var activeSounds:Vector.<SoundData>;
		private var dataToFade:SoundData;

		
		public function SamplePlayer() 
		{
			// initialize members:
			registeredSounds = new Vector.<SoundData>();
			activeSounds = new Vector.<SoundData>();
		}		
		
		
		/**
		 * Extracts the sound's sample data and stores it to be played under the specified ID.
		 * If a sound with this ID is already registered, this function does nothing.
		 */
		public function registerSound(id:String, sound:Sound):void
		{			
			// create sound data object:
			var newSound:SoundData = new SoundData();
			newSound.id = id;
			newSound.sound = sound;
			registeredSounds.push(newSound);
		}
		
		/*
		Returns the SoundData registered with the given id, or null if none was registered.
		*/
		private function getSoundData(id:String):SoundData
		{
			for each (var data:SoundData in registeredSounds)
			{
				if (data.id == id) return data;
			}
			return null;
		}
		
		/**
		 * Plays the sound specified by the given ID. You need to call registerSound() first in
		 * order to set up the ID.
		 * You can specify an optional callback function that will be called immediately when the 
		 * sound completes. If the callback needs any arguments, you can optionally specify them as
		 * an array.
		 * If the ID you pass isn't registered no sound will play, and if any callback is specified,
		 * it will be executed immediately. 
		 */
		public function playSound(id:String, looping:Boolean = false, volume:Number = 1.0,  ):SoundData
		{
			
			trace("SamplePlayer: playing ID: "+id);
			// get the sound data object registered for this id:
			var data:SoundData = getSoundData(id);
			if (!data) // sound has not been registered
			{
				trace("SamplePlayer: Sound id \""+id+"\" has not been registered!");
				return null;
			}
			var soundTransform:SoundTransform = new SoundTransform( vol );
			// copy the sound data object:
			data = data.shallowCopy();	
			var dataChannel:SoundChannel;
			if ( looping )
				dataChannel = data.sound.play(0, 2147000000);
			else
				dataChannel = data.sound.play();
			data.channel = dataChannel;
			data.channel.addEventListener(Event.SOUND_COMPLETE, onSoundChannelSoundComplete);
			
			activeSounds.push(data);
			return data;
		}		
		
		private function onSoundChannelSoundComplete(e:Event):void 
		{
			ctiveSounds.splice( activeSounds.indexOf(e.target), 1);
		}
		
		
		
		
		/*
		 * This stops the sound specified by Id 
		 */
		public function stopSoundById(id:String):void 
		{
			var data:SoundData = getSoundData(id);
			//trace("Sound Id:" + id + ". Data: " + data );
			if (!data) // sound has not been registered
			{
				return;
			}		
			
			for ( var entry:*  in activeSounds)
			{
				var aSound:SoundData = activeSounds[entry];
				if ( aSound.id == id )
				{
					trace("SamplePlayer: sound \"" + aSound.id + "\" was stopped by user.");
					aSound.sound.close();
					activeSounds.splice( activeSounds.indexOf(aSound), 1); //removes sound from active
					return;
				}
			}			
			//activeSounds = activeSounds.concat( activeSounds.slice(0, rmIndex-1), activeSounds.slice(rmIndex+1, activeSounds.length-1 ) );			
		}
		
		/*
		 * This stops the sound specified by SoundData object
		 */
		public function stopSoundByData(sData:SoundData):void 
		{			
			
			if (!sData) // sound has not been registered
			{
				return;
			}		
			
			for ( var entry:*  in activeSounds)
			{
				var aSound:SoundData = activeSounds[entry];
				if ( aSound == sData  )
				{
					trace("SamplePlayer: sound \"" + aSound.id + "\" was stopped by user.");
					aSound.sound.close();
					activeSounds.splice( activeSounds.indexOf(aSound), 1); //removes sound from active
					return;
				}
			}			
			//activeSounds = activeSounds.concat( activeSounds.slice(0, rmIndex-1), activeSounds.slice(rmIndex+1, activeSounds.length-1 ) );			
		}
		
		/*
		 * This stops all sounds in current sample player instance
		 */
		public function stopAllSounds():void
		{			
			for each (var data:SoundData in activeSounds)
			{
				data.sound.close();
			}
			activeSounds = new Vector.<SoundData>;
		}
		
		/*
		 * Stops all sounds except one specified by id
		 */
		
		public function stopAllExceptIds( sIds:Array ):void
		{
			//trace("Stop all except: " + sIds);
			for each (var data:SoundData in activeSounds)
			{
				
				if ( sIds.indexOf( data.id ) == -1 )
				{
					//trace("Data from registersonds: " + data.id + " removed.");
					//Fade sound
					//activeSounds.splice( activeSounds.indexOf(data), 1);					
					dataToFade = data;
					fadePhase = 100;
					fadeTimer = new Timer( 100);
					fadeTimer.start()
					fadeTimer.addEventListener(TimerEvent.TIMER, onFade);
					
				}
			}
		}
		
		private function onFade(e:TimerEvent):void 
		{
			fadePhase -= 1;
			dataToFade.channel.soundTransform = new SoundTransform( 0.8 );
			if ( fadePhase <= 0 )
			{
				dataToFade.sound.close();
				activeSounds.splice( activeSounds.indexOf(dataToFade), 1);
				e.target.stop();
			}
		}
		
		/*
		 * Gives Array of strings - IDs for each registered sample player 
		 * 
		 */
		public function getRegisteredIDs():Vector.<String>
		{
			var result:Vector.<String> = new Vector.<String>();
			for each (var data:SoundData in registeredSounds)
			{
				result.push(data.id);
			}
			return result;
		}
		
		/*
		 * Sets volume for some sample denoted by SoundData object
		 */
		
		public function setVolume( sData:SoundData, vol:Number ):void
		{
			if (!sData) // sound has not been registered
			{
				return;
			}
			
			sData.channel.soundTransform = new SoundTransform( vol );		
		}
		
		/*
		 * Informs user whether at least some sample is active at the moment
		 */
		public function isPlaying():Boolean
		{
			return activeSounds.length > 0;
		}
		
		/*
		 * If specific sound specified by id is currently playing, returns SoundData of this object
		 */
		public function isIdPlaying( sId:String ):SoundData
		{
			for each ( var sData:SoundData in activeSounds )
			{
				if ( sData.id == sId )
					return sData;
			}
			return null;
		}
	}

}