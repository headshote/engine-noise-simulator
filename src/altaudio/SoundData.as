package altaudio 
{
	import flash.media.Sound;
	import flash.media.SoundChannel;
	/**
	 * ...
	 * @author Hammerzeit
	 */
	
	/*
	Class containing sample data and information for a sound. This also 
	stores an optional callback with arguments that will be called when the sound playback 
	completes.
	*/
	public class SoundData
	{
		public var id:String;
		public var sound:Sound;
		public var channel:SoundChannel;
		
		// returns a shallow clone of a sound data object
		public function shallowCopy():SoundData
		{
			var result:SoundData = new SoundData();
			result.id = id;
			result.sound = sound;			
		}
	}

}