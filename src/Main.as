package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import program.Program;
	
	/**
	 * ...
	 * @author Hammerzeit
	 */
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			//var testarr:Array = [ "213", "213412341234", "asd", "aa"];
			//trace( (testarr.indexOf("213") !=-1 ) );
			addChild( new Program() );
		}
		
	}
	
}