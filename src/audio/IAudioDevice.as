package  audio
{
	
	/**
	 * ...
	 * @author Hammerzeit
	 */
	public interface IAudioDevice 
	{
		function process( leftOutput:Vector.<Number>, rightOutput:Vector.<Number> ):void;
	}
	
}