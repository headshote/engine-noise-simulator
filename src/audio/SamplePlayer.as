package audio 
{
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	/**
	 * ...
	 * @author Hammerzeit
	 */
	public class SamplePlayer implements IAudioDevice 
	{
		// Vector of registered SoundData instances:
		private var registeredSounds:Vector.<SoundData>;
		// Vector of active SoundData instances:
		private var activeSounds:Vector.<SoundData>;

		// current index into the output buffer. If a sound with a completion callback
		// ends at some point between buffer start and buffer end, this will be between
		// 0 and BUFFER_SIZE.
		private var currentOutputBufferPosition:int = 0;
		
		private var fadeTimer:Timer;
		private var fadePhase:int;
		private var dataToFade:SoundData;
		
		//private var playingIds:Vector.<String>;		
		
		public function SamplePlayer() 
		{
			// initialize members:
			registeredSounds = new Vector.<SoundData>();
			activeSounds = new Vector.<SoundData>();
			//playingIds = new Vector.<String>();
		}
		
		
		/* INTERFACE IAudioDevice */
		
		public function process(leftOutput:Vector.<Number>, rightOutput:Vector.<Number>):void 
		{
			var i:int;
			var bufferSize:int = leftOutput.length;
			
			// go over all currently active SoundData objects and add their data to the buffers:
			for (var j:int = 0; j<activeSounds.length; j++) 
			{
				currentOutputBufferPosition = 0;
				
				var currentSound:SoundData = activeSounds[j];
				var leftData:Vector.<Number> = currentSound.leftData;
				var rightData:Vector.<Number> = currentSound.rightData;
				var volume:Number = currentSound.volume;
				var outputStartIndex:int = 0;
				
				// If the currentSound's position is negative, the sound hasn't started yet.
				// In this case, set the start index to the point at which the sound starts:
				if (currentSound.position < 0)
				{
					outputStartIndex = -currentSound.position;
					currentSound.position = 0;
				}
				
				var outputEndIndex:int = bufferSize;
				var soundEndsDuringThisCycle:Boolean = false;
				var remainingSamples:int = currentSound.length-currentSound.position;
				if (remainingSamples-outputStartIndex <= bufferSize)
				{
					// sound will complete within this cycle.
					if ( !currentSound.looping )
					{
						outputEndIndex = remainingSamples;
						soundEndsDuringThisCycle = true;
					}
					else
					{
						//outputEndIndex = remainingSamples;
						currentSound.position = 0;
						soundEndsDuringThisCycle = false;
					}
					
				}
				
				// add samples to the mix:
				var dataPos:int;
				
				dataPos = currentSound.position;
				for (i=outputStartIndex; i<outputEndIndex; i++)
				{
					leftOutput[i] += leftData[dataPos] * volume;
					dataPos++;
				}
				dataPos = currentSound.position;
				for (i=outputStartIndex; i<outputEndIndex; i++)
				{
					rightOutput[i] += rightData[dataPos] * volume;
					dataPos++;
				}
				currentSound.position = dataPos;
				
				// if sound has completed:
				currentOutputBufferPosition = outputEndIndex;
				if (soundEndsDuringThisCycle)
				{
					trace("SamplePlayer: sound \""+currentSound.id+"\" is ending. Current buffer position: "+currentOutputBufferPosition);
					// remove the sound from activeSounds
					activeSounds.splice(j, 1);
					j--;
					// and call its callback (if any)
					if (currentSound.completionCallback != null)
					{
						if (currentSound.callbackArgs == null)
						{
							currentSound.completionCallback.call(this);
						} else 
						{
							//trace(currentSound.callbackArgs + " type - array: " + (currentSound.callbackArgs is Array));
							//currentSound.completionCallback.call(this, currentSound.callbackArgs[0], currentSound.callbackArgs[1], currentSound.callbackArgs[2], currentSound.callbackArgs[3]);
							currentSound.completionCallback.apply(this, currentSound.callbackArgs );
						}
					}
				}
			}	
		}
		
		/**
		 * Extracts the sound's sample data and stores it to be played under the specified ID.
		 * If a sound with this ID is already registered, this function does nothing.
		 */
		public function registerSound(id:String, sound:Sound):void
		{
			trace("SamplePlayer: Registering sound with id \""+id+"\".");
			if (getSoundData(id) != null)
			{
				trace("SamplePlayer: Sound id \""+id+"\" already exists!");
				return;
			}
			
			// extract sound into ByteArray: 
			var samples:ByteArray = new ByteArray();
			sound.extract(samples, sound.length*44100, 0);
			samples.position = 0;
			var numSamples:int = samples.length/(4*2); // 4 bytes per value, 1 value per stereo channel
			
			// copy the samples from ByteArray into Vector.<Number>s:
			var leftChannel:Vector.<Number> = new Vector.<Number>();
			var rightChannel:Vector.<Number> = new Vector.<Number>();
			var i:int = 0;
			while (samples.bytesAvailable) 
			{
				leftChannel[i] = samples.readFloat();
				rightChannel[i] = samples.readFloat();
				i++;
			}
			
			// create sound data object:
			var newSound:SoundData = new SoundData();
			newSound.id = id;
			newSound.leftData = leftChannel.concat();
			newSound.rightData = rightChannel.concat();
			newSound.volume = 1.0;
			newSound.position = 0;
			newSound.length = leftChannel.length;
			registeredSounds.push(newSound);
		}
		
		/*
		Returns the SoundData registered with the given id, or null if none was registered.
		*/
		private function getSoundData(id:String):SoundData
		{
			for each (var data:SoundData in registeredSounds)
			{
				if (data.id == id) return data;
			}
			return null;
		}
		
		/**
		 * Plays the sound specified by the given ID. You need to call registerSound() first in
		 * order to set up the ID.
		 * You can specify an optional callback function that will be called immediately when the 
		 * sound completes. If the callback needs any arguments, you can optionally specify them as
		 * an array.
		 * If the ID you pass isn't registered no sound will play, and if any callback is specified,
		 * it will be executed immediately. 
		 */
		public function playSound(id:String, looping:Boolean = false, volume:Number = 1.0,  completionCallback:Function = null, callbackArgs:Array = null):SoundData
		{			
			trace("SamplePlayer: playing ID: "+id);
			// get the sound data object registered for this id:
			var data:SoundData = getSoundData(id);
			if (!data) // sound has not been registered
			{
				trace("SamplePlayer: Sound id \""+id+"\" has not been registered!");
				if (completionCallback != null)
				{
					completionCallback.apply(this, callbackArgs);
				}
				return null;
			}
			
			//playingIds.push(id);
			
			// copy the sound data object:
			data = data.shallowCopy();			
			if ( looping )
			{
				data.looping = true;
				data.completionCallback = playSound;
				data.callbackArgs = [id, looping, volume, null, null];
			}
			else
			{
				data.looping = false;
				data.completionCallback = completionCallback;
				data.callbackArgs = callbackArgs;
			}
			data.position = -currentOutputBufferPosition;
			data.volume = volume;
			activeSounds.push(data);
			return data;
		}
		
		/**
		 * Takes an Array of IDs (Strings) and plays back the corresponding Sounds.
		 * You can specify an optional callback function that will be called immediately when the 
		 * sequence completes. If the callback needs any arguments, you can optionally specify them as
		 * an array.
		 */
		public function playSequence(ids:Array, completionCallback:Function = null, callbackArgs:Array = null):void
		{
			if (ids.length == 0) return;
			
			ids = ids.concat();
			var initialID:String = ids.shift();
			
			playSound(initialID, false, 1.0, playSequenceHelper_(ids, completionCallback, callbackArgs), callbackArgs);
		}
		
		/*
		Recursive method. Creates a chain of callbacks from the supplied array, each of which
		calls playSound with its corresponding ID and sets up the next callback to be called
		upon completion of the sound.
		*/
		private function playSequenceHelper_(ids:Array, finalCallback:Function, callbackArgs:Array = null):Function
		{
			if (ids.length == 0) return finalCallback;
			var currentID:String = ids.shift();
			return function():* {
				playSound(currentID, false, 1.0, playSequenceHelper_(ids, finalCallback, callbackArgs), callbackArgs);
			};
		}
		
		/*
		 * This stops the sound specified by Id 
		 */
		public function stopSoundById(id:String):void 
		{
			var data:SoundData = getSoundData(id);
			//trace("Sound Id:" + id + ". Data: " + data );
			if (!data) // sound has not been registered
			{
				return;
			}		
			
			for ( var entry:*  in activeSounds)
			{
				var aSound:SoundData = activeSounds[entry];
				if ( aSound.id == id )
				{
					trace("SamplePlayer: sound \"" + aSound.id + "\" was stopped by user.");
					//playingIds.splice( playingIds.indexOf(id), 1 );
					activeSounds.splice( activeSounds.indexOf(aSound), 1); //removes sound from active
					return;
				}
			}			
			//activeSounds = activeSounds.concat( activeSounds.slice(0, rmIndex-1), activeSounds.slice(rmIndex+1, activeSounds.length-1 ) );			
		}
		
		/*
		 * This stops the sound specified by SoundData object
		 */
		public function stopSoundByData(sData:SoundData):void 
		{			
			
			if (!sData) // sound has not been registered
			{
				return;
			}		
			
			for ( var entry:*  in activeSounds)
			{
				var aSound:SoundData = activeSounds[entry];
				if ( aSound == sData  )
				{
					trace("SamplePlayer: sound \"" + aSound.id + "\" was stopped by user.");
					//playingIds.splice( playingIds.indexOf(sData.id), 1 );
					activeSounds.splice( activeSounds.indexOf(aSound), 1); //removes sound from active
					return;
				}
			}			
			//activeSounds = activeSounds.concat( activeSounds.slice(0, rmIndex-1), activeSounds.slice(rmIndex+1, activeSounds.length-1 ) );			
		}
		
		/*
		 * This stops all sounds in current sample player instance
		 */
		public function stopAllSounds():void
		{
			//playingIds = null;
			activeSounds = null;
			//playingIds = new Vector.<String>;
			activeSounds = new Vector.<SoundData>;
		}
		
		/*
		 * Stops all sounds except one specified by id
		 */
		
		public function muteAllExceptIds( sIds:Array ):void
		{
			//trace("Stop all except: " + sIds);
			for each (var data:SoundData in activeSounds)
			{
				
				if ( sIds.indexOf( data.id ) == -1 )
				{
					//trace("Data from registersonds: " + data.id + " removed.");
					//Fade sound
					//activeSounds.splice( activeSounds.indexOf(data), 1);
					data.volume *= 0.5;
					//data.completionCallback = null;
					//data.callbackArgs = null;
					dataToFade = data;
					fadePhase = 100;
					fadeTimer = new Timer( 100);
					fadeTimer.start()
					fadeTimer.addEventListener(TimerEvent.TIMER, onFade);
					
				}
			}
		}
		
		private function onFade(e:TimerEvent):void 
		{
			fadePhase -= 1;
			dataToFade.volume *= 0.5
			if ( fadePhase <= 0 )
			{
				dataToFade.volume = 0;
				e.target.stop();
			}
		}
		
		/*
		 * Gives Array of strings - IDs for each registered sample player 
		 * 
		 */
		public function getRegisteredIDs():Vector.<String>
		{
			var result:Vector.<String> = new Vector.<String>();
			for each (var data:SoundData in registeredSounds)
			{
				result.push(data.id);
			}
			return result;
		}
		
		/*
		 * Sets volume for some sample denoted by SoundData object
		 */
		
		public function setVolume( sData:SoundData, vol:Number ):void
		{
			if (!sData) // sound has not been registered
			{
				return;
			}
			
			sData.volume = vol;		
		}
		
		/*
		 * Informs user whether at least some sample is active at the moment
		 */
		public function isPlaying():Boolean
		{
			return activeSounds.length > 0;
		}
		
		/*
		 * If specific sound specified by id is currently playing, returns SoundData of this object
		 */
		public function isIdPlaying( sId:String ):SoundData
		{
			//if ( playingIds.indexOf(sId) == -1)
			//	return null;
			for each ( var sData:SoundData in activeSounds )
			{
				if ( sData.id == sId )
					return sData;
			}
			return null;
		}
	}

}