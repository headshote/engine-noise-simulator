package audio 
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.SampleDataEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.ByteArray;
	/**
	 * ...
	 * @author Hammerzeit
	 */	
		
	public class SoundEngine extends EventDispatcher 
	{		
		// AudioEngine is a Singleton - only one instance can exist at a time.
		private static var instance_:SoundEngine;
		
		// global buffer size:
		public static const BUFFER_SIZE:int = 4096;

		// Sound object instance we'll be using to output the audio mix:
		private var output:Sound;

		// Vectors containing the left and right buffers.
		private var currentLeftOutput:Vector.<Number>;
		private var currentRightOutput:Vector.<Number>;
		
		private var _channel:SoundChannel;

		public var inputs:Vector.<IAudioDevice>;
		
		/**
		 * Returns the shared AudioEngine instance. Use this instead of the constructor.
		 */ 
		public static function get instance():SoundEngine
		{
			if (!instance_)
			{
				instance_ = new SoundEngine(new SingletonEnforcer());
			}
			return instance_;
		}
				
		public function SoundEngine( singletonEnforcer:SingletonEnforcer) 
		{
			super(null);

			if (!singletonEnforcer)
			{
				throw new Error("AudioEngine is a Singleton. Use \"AudioEngine.instance\" instead of \"new AudioEngine()\"!"); 
			}
			
			// initialize members:
			currentLeftOutput = new Vector.<Number>(BUFFER_SIZE);
			currentRightOutput = new Vector.<Number>(BUFFER_SIZE);
			
			//Initialize input vector
			inputs = new Vector.<IAudioDevice>();
			
			// start output:
			output = new Sound();
			output.addEventListener(SampleDataEvent.SAMPLE_DATA, sampleDataHandler, false, 0, true);
			_channel = output.play();					
		}
		
		/*
		 * Volume setter
		 */
		public function changeVolume( vol:Number ):void
		{
			_channel.soundTransform = new SoundTransform( vol );
		}
		
		
		/*
		Called whenever the output sound instance needs new samples. Mixes sample data
		from all active SoundData instances together, and checks which SoundData instances
		have finished playing, calling callbacks where applicable. 
		*/
		private function sampleDataHandler(e:SampleDataEvent):void 
		{
			var i:int;
			
			// clear buffer vectors:
			for (i=0; i<BUFFER_SIZE; i++)
			{
				currentLeftOutput[i] = 0;
				currentRightOutput[i] = 0;
			}
			
			// process input device:
			for each ( var input:IAudioDevice in inputs )
			{
				if (input != null)
				{	
					input.process(currentLeftOutput, currentRightOutput);
				}
			}
			
			// copy buffers to sample data ByteArray from event:
			for (i=0; i<BUFFER_SIZE; i++)
			{
				/*if ( currentLeftOutput[i] !=0 && currentRightOutput[i] !=0)
					trace("Sample Data: "+currentLeftOutput[i]+" (left), "+currentRightOutput[i]+" (right).");*/
				e.data.writeFloat(currentLeftOutput[i]);
				e.data.writeFloat(currentRightOutput[i]);
			}
		}
		
	}	
}

class SingletonEnforcer
{
}