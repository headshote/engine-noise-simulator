package audio 
{
	/**
	 * ...
	 * @author Hammerzeit
	 */
	
	/*
	Class containing sample data and information for a sound. This also 
	stores an optional callback with arguments that will be called when the sound playback 
	completes.
	*/
	public class SoundData
	{
		public var id:String;
		public var leftData:Vector.<Number>;
		public var rightData:Vector.<Number>;
		
		public var position:int = 0;
		public var length:int;
		
		public var completionCallback:Function = null
		public var callbackArgs:Array;
		
		public var volume:Number = 1.0;
		public var looping:Boolean;
		
		// returns a shallow clone of a sound data object
		public function shallowCopy():SoundData
		{
			var result:SoundData = new SoundData();
			result.id = id;
			result.leftData = leftData;
			result.rightData = rightData;
			result.length = length;
			result.completionCallback = completionCallback;
			result.callbackArgs = callbackArgs;
			return result;
		}
	}

}