package program 
{
	import audio.SamplePlayer;
	import audio.SoundData;
	import audio.SoundEngine;
	import fl.controls.Button;
	import fl.controls.CheckBox;
	import fl.controls.Slider;
	import fl.events.SliderEvent;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Hammerzeit
	 */
	public class Program extends Sprite
	{
		private var ui:UI_mc;
		private var samplePlayer:SamplePlayer;
		private var sampleSimPlayer:SamplePlayer;
		private var soundEngine:SoundEngine;
		private var volume:Number;
		private var speed:Number;
		private var looping:Boolean;
		private var preRecordedRPMs:Vector.<Number>;
		private var currentSample:SoundData;
		private var simulate:Boolean;
		private var frameCount:int;
		
		public function Program() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			preRecordedRPMs = new Vector.<Number>();
			
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			ui = new UI_mc();
			addChild(ui);
			
			//trace( ui.getChildByName("sldVolume") );
			
			addEventListener(Event.ENTER_FRAME, update );
			
			soundEngine = SoundEngine.instance;
			samplePlayer = new SamplePlayer();
			//sampleSimPlayer = new SamplePlayer();
			soundEngine.inputs.push( samplePlayer );
			//soundEngine.inputs.push( sampleSimPlayer );
						
			samplePlayer.registerSound("rpm_859", new rpm_859_P() );
			samplePlayer.registerSound("rpm_919", new rpm_919_P() );
			samplePlayer.registerSound("rpm_984", new rpm_984_P() );
			samplePlayer.registerSound("rpm_1052", new rpm_1052_P() );
			samplePlayer.registerSound("rpm_1126", new rpm_1126_P() );
			samplePlayer.registerSound("rpm_1205", new rpm_1205_P() );
			samplePlayer.registerSound("rpm_1289", new rpm_1289_P() );
			samplePlayer.registerSound("rpm_1380", new rpm_1380_P() );
			samplePlayer.registerSound("rpm_1476", new rpm_1476_P() );
			samplePlayer.registerSound("rpm_1579", new rpm_1579_P() );
			samplePlayer.registerSound("rpm_1690", new rpm_1690_P() );
			samplePlayer.registerSound("rpm_1808", new rpm_1808_P() );
			samplePlayer.registerSound("rpm_1935", new rpm_1935_P() );
			samplePlayer.registerSound("rpm_2070", new rpm_2070_P() );
			samplePlayer.registerSound("rpm_2215", new rpm_2215_P() );
			samplePlayer.registerSound("rpm_2370", new rpm_2370_P() );
			samplePlayer.registerSound("rpm_2536", new rpm_2536_P() );
			samplePlayer.registerSound("rpm_2714", new rpm_2714_P() );
			samplePlayer.registerSound("rpm_2904", new rpm_2904_P() );
			samplePlayer.registerSound("rpm_3107", new rpm_3107_P() );
			samplePlayer.registerSound("rpm_3324", new rpm_3324_P() );
			samplePlayer.registerSound("rpm_3557", new rpm_3557_P() );
			samplePlayer.registerSound("rpm_3806", new rpm_3806_P() );
			samplePlayer.registerSound("rpm_4073", new rpm_4073_P() );
			samplePlayer.registerSound("rpm_4358", new rpm_4358_P() );
			samplePlayer.registerSound("rpm_4663", new rpm_4663_P() );
			samplePlayer.registerSound("rpm_4989", new rpm_4989_P() );
			samplePlayer.registerSound("rpm_5338", new rpm_5338_P() );
			samplePlayer.registerSound("rpm_5712", new rpm_5712_P() );
			samplePlayer.registerSound("rpm_6112", new rpm_6112_P() );
			samplePlayer.registerSound("rpm_6540", new rpm_6540_P() );
			
			/*
			samplePlayer.registerSound("rpm_803", new rpm_803_P() );
			samplePlayer.registerSound("rpm_859", new rpm_859_P() );
			samplePlayer.registerSound("rpm_919", new rpm_919_P() );
			samplePlayer.registerSound("rpm_984", new rpm_984_P() );
			samplePlayer.registerSound("rpm_1052", new rpm_1052_P() );
			samplePlayer.registerSound("rpm_1126", new rpm_1126_P() );
			samplePlayer.registerSound("rpm_1205", new rpm_1205_P() );
			samplePlayer.registerSound("rpm_1289", new rpm_1289_P() );
			samplePlayer.registerSound("rpm_1380", new rpm_1380_P() );
			samplePlayer.registerSound("rpm_1476", new rpm_1476_P() );
			samplePlayer.registerSound("rpm_1579", new rpm_1579_P() );
			samplePlayer.registerSound("rpm_1690", new rpm_1690_P() );
			samplePlayer.registerSound("rpm_1690", new rpm_1690_P() );
			samplePlayer.registerSound("rpm_1808", new rpm_1808_P() );
			samplePlayer.registerSound("rpm_1935", new rpm_1935_P() );
			*/
			
			//trace( samplePlayer.getRegisteredIDs() );
			var ids:Vector.<String> = samplePlayer.getRegisteredIDs();
			//trace( ids.length );
			var regex:RegExp = /\d[0-9][0-9]*/g;
			for each ( var id:String in ids )
			{
				//trace(id);
				//trace( id.match(regex) );
				preRecordedRPMs.push( Number(id.match(regex)) );
			}
			trace(preRecordedRPMs);
			
			(ui.getChildByName("btnPlay") as Button).addEventListener(MouseEvent.MOUSE_DOWN, onPlayClick);
			(ui.getChildByName("btnStop") as Button).addEventListener(MouseEvent.MOUSE_DOWN, onStopClick);
			(ui.getChildByName("sldVolume") as Slider).addEventListener(SliderEvent.CHANGE, onVolumeChange);
			(ui.getChildByName("sldSpeed") as Slider).addEventListener(SliderEvent.CHANGE, onSpeedChange);			
			(ui.getChildByName("chkLoop") as CheckBox).addEventListener(MouseEvent.CLICK, onchkLoopClick);
			(ui.getChildByName("btnSimulate") as Button).addEventListener(MouseEvent.MOUSE_DOWN, onSimClick);
			(ui.getChildByName("btnStopSim") as Button).addEventListener(MouseEvent.MOUSE_DOWN, onStopSimClick);
			
			looping = (ui.getChildByName("chkLoop") as CheckBox).selected;
			speed = ( ui.getChildByName("sldSpeed") as Slider).value  * 1000;
			volume = ( ui.getChildByName("sldVolume") as Slider).value;
			simulate = false;
		}	
		
		/*** EVENTS ***/
		private function onSimClick(e:MouseEvent):void 
		{
			if ( !simulate )
			{
				initSimulation();
				simulateEngineNoise( speed );				
				simulate = true;
			}
		}		
		
		private function initSimulation():void 
		{
			for each ( var rpmId:String in preRecordedRPMs )
				samplePlayer.playSound("rpm_"+rpmId, true, 0);
		}
		
		
		private function onStopSimClick(e:MouseEvent):void 
		{
			simulate = false;
		}
		
		private function onStopClick(e:MouseEvent):void 
		{
			//samplePlayer.stopAllSounds();
			samplePlayer.stopSoundByData(currentSample );
		}
		
		private function onchkLoopClick(e:MouseEvent):void 
		{
			looping = (ui.getChildByName("chkLoop") as CheckBox).selected;
		}
		
		private function onSpeedChange(e:SliderEvent):void 
		{
			speed = ( ui.getChildByName("sldSpeed") as Slider).value * 1000;
			//samplePlayer.stopSoundByData(currentSample );
			//currentSample = samplePlayer.playSound("rpm_"+String(getClosestPrerecorded()), false, volume, startMainLoop);
			//trace( "Speed: " + speed + ". Closest prerecorded: " + getClosestPrerecorded() );
		}
		
		private function onVolumeChange(e:SliderEvent):void 
		{
			volume = ( ui.getChildByName("sldVolume") as Slider).value;
			soundEngine.changeVolume(volume);
			//samplePlayer.setVolume(currentSample, volume);
		}
		
		private function onPlayClick(e:MouseEvent):void 
		{
			//samplePlayer.playSound("rpm_4073", true);
			currentSample = samplePlayer.playSound("rpm_"+String(getClosestPrerecorded()), false, volume, startMainLoop);
		}
		
		
		/*** Private class methods ***/
		
		/* 
		 * to test callbacks
		 */
		private function startMainLoop():void 
		{
			if ( looping )
			{
				currentSample = samplePlayer.playSound("rpm_"+String(getClosestPrerecorded()), false, volume, startMainLoop);
			}
			else
			{
				;
			}
			
		}
		
		/*
		 * Get closest value to the set of prerecorded RPMs
		 */
		private function getClosestPrerecorded():Number
		{
			var best:Number = NaN;
			for each ( var rpm:Number in preRecordedRPMs )
			{
				if ( isNaN(best) )
				{
					best = rpm;
				}
				else
				{
					if ( Math.abs( rpm - speed ) < Math.abs( best - speed) )
					{
						best = rpm;
					}
				}
			}
			return best;
		}
		
		private function update(e:Event):void 
		{	
			frameCount = (frameCount + 1) % 30;
			speed = ( ui.getChildByName("sldSpeed") as Slider).value * 1000;	
			//trace(speed);
			if ( simulate )
			{
				simulateEngineNoise( speed );
			}
			
			//trace(samplePlayer.isPlaying());
			
			/*if ( samplePlayer.isIdPlaying("rpm_859") )
				trace("true");
			else
				trace("false");*/
		}
		
		private function simulateEngineNoise(currentSpeed:Number):void 
		{
			var prerecorded_min:Number;
			var prerecorded_max:Number;
			
			//Sort vector of prerecorded sounds for further processing
			var sortMethod:Function = function (a:Number, b:Number) : int
			{
				var result:int;
				if (a < b) {
					result = -1;
				} else if (a > b) {
					result =  1;    
				} else {
					result 0;
				}
				//trace(a+"\t"+b +"\t: "+ result);
				return result;
			}
			preRecordedRPMs.sort(sortMethod);			
			//trace(preRecordedRPMs);
			
			//Determine min and max prerecorded rpms
			prerecorded_min = preRecordedRPMs[0];
			prerecorded_max = preRecordedRPMs[ preRecordedRPMs.length -1];			
			//trace("Prerecorded max: " + prerecorded_max + ". Prerecorded min: " + prerecorded_min);
			
			//Main logic
			if ( currentSpeed < prerecorded_min )
			{
				//Just play min rpm sound at 100% volume here
				samplePlayer.muteAllExceptIds( ["rpm_" + prerecorded_min] );
				playPrerecordedRPMSound( prerecorded_min, samplePlayer, 1.0);				
				
			}
			else if ( currentSpeed > prerecorded_max )
			{
				//Just play max rpm sound at 100% volume
				samplePlayer.muteAllExceptIds( ["rpm_" + prerecorded_max] );
				playPrerecordedRPMSound( prerecorded_max, samplePlayer, 1.0);				
			}
			else
			{
				//Play 2 sounds simultaneously at different volume levels
				//Sort of like cross fading
				
				//1. Determine in what range of prerecorded sounds current speed lies
				var range:Vector.<Number> = determineRange( speed );
				var lowerBound:Number = range[0];
				var upperBound:Number = range[1];				
				//trace("Speed: " + currentSpeed + " is in range: [" + range[0] + "; " + range[1] + "].");
				
				//2. Calculate volumes for each of two prerecorded sounds from that range
				var volumes:Vector.<Number> = calculateVolumes( currentSpeed, lowerBound, upperBound);
				var lowerVol:Number = volumes[0];
				var upperVol:Number = volumes[1];
				
				//trace("Speed: " + currentSpeed + ". LowerBound volume: " + volumes[0] + ". Upperbound volume: " + volumes[1] + ".");
				
				//3. Play these sounds at that volumes
				samplePlayer.muteAllExceptIds( ["rpm_" + lowerBound, "rpm_" + upperBound] );
				playPrerecordedRPMSound( lowerBound, samplePlayer, lowerVol);
				playPrerecordedRPMSound( upperBound, samplePlayer, upperVol);
			}
		}
		
		private function playPrerecordedRPMSound(rmpId:Number, samplePlayer:SamplePlayer, vol:Number):void 
		{
			var sData:SoundData = samplePlayer.isIdPlaying("rpm_" + rmpId);
			//If sound of this id is already playing, just twitch the volume (100%)
			if ( sData )
			{
				samplePlayer.setVolume(sData, vol );
			}
			//Else play it at some volume
			else
			{
				//samplePlayer.stopSoundByData( currentSample );
				//samplePlayer.stopAllExceptId("rpm_" + rmpId);
				//trace("play prerecorded rpm sound called on: " + rmpId + ". At frame: " + frameCount);
				//samplePlayer.playSound("rpm_"+rmpId, true, vol);
			}
		}
		
		private function calculateVolumes(currentSpeed:Number, lowerBound:Number, upperBound:Number):Vector.<Number> 
		{
			var lowerBoundVolume:Number;
			var upperBoundVolume:Number;
			var interval:Number;
			var result:Vector.<Number>;
			
			interval = Math.abs( upperBound - lowerBound );
					
			lowerBoundVolume = ( (interval) - Math.abs( currentSpeed - lowerBound ) ) / (interval);
			upperBoundVolume = ( (interval) - Math.abs( currentSpeed - upperBound ) ) / (interval);
			
			result = new Vector.<Number>();
			result.push(lowerBoundVolume);
			result.push(upperBoundVolume);
			return result;
		}
		
		/*
		 * Determines in what range of prerecorded rpms is current speed lying
		 */
		private function determineRange(currentSpeed:Number):Vector.<Number> 
		{
			//1. Find closest smaller or equal			
			//2. Find closest bigger or equal
			var lowerBound:Number = preRecordedRPMs[0];
			var upperBound:Number = preRecordedRPMs[ preRecordedRPMs.length - 1 ];
			var result:Vector.<Number>;
			
			for each ( var rpm:Number in preRecordedRPMs )
			{				
				//carry on with scanning
				
				if ( rpm >= currentSpeed && rpm < upperBound)
				{
					///For upper bound
					upperBound = rpm;
					
				}
				else if ( rpm <= currentSpeed && rpm > lowerBound)
				{
					///For lower bound
					lowerBound = rpm;
				}
				
			}
			
			result = new Vector.<Number>();
			result.push(lowerBound);
			result.push(upperBound);
			
			return 	result;
		}
		
	}

}